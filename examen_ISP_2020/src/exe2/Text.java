package exe2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Text extends JFrame {

    JLabel labelCamp, labelZona;
    JTextField textField;
    JTextArea textArea;
    JButton button;

    public Text() {
        setTitle("Introducere text");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        setLayout(null);
        setSize(400, 400);

        init();
    }

    public void init() {
        int width = 70;
        int height = 20;

        labelCamp = new JLabel("Camp text:");
        labelCamp.setBounds(10, 10, width, height);

        textField = new JTextField();
        textField.setBounds(80, 10, width, height);

        labelZona = new JLabel("Zona text:");
        labelZona.setBounds(10, 40, width, height);

        textArea = new JTextArea();
        textArea.setBounds(80, 40, 150, 100);

        button = new JButton("START");
        button.setBounds(10, 150, 100, height);
        button.addActionListener(new TratareButon());

        add(labelCamp);
        add(textField);
        add(labelZona);
        add(textArea);
        add(button);
    }

    class TratareButon implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            try {
                String text = textField.getText();
                textArea.setText(text);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        Text text = new Text();
    }

}
