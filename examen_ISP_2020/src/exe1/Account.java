package exe1;

import java.util.ArrayList;

public class Account {
    Card card;

    public Account(Card card) {
        this.card = card;
    }
}

class Card {

}

class Transaction {
    private int amount;
    Account account1;
    Account account2;

    public Transaction(Account account1, Account account2) {
        this.account1 = account1;
        this.account2 = account2;
    }
}

class Bank {
    ArrayList<Account> accounts = new ArrayList<Account>();
    ArrayList<User> users;

    public Bank(ArrayList<Account> account) {
        this.accounts = account;
    }

    public void met1(User user) {
        users.add(user);
    } // metoda a fost creata pentru a evidentia asocierea
}

class User {
    ArrayList<Account> accounts;

    public void met2(Account account) {
        accounts.add(account);
    } // metoda a fost creata pentru a evidentia asocierea

}

